class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to user
    else
      flash.now[:danger] = 'Nom et/ou mot de passe invalide(s)'
      render new
    end
  end

  def destroy
    logout
    redirect_to_root_url
  end
end
