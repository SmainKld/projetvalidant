class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new
    @user.name = params_user[:name]
    if @user.save
      log_in @user
      flash[:success] = "Bienvenue sur EventBriteLike!"
      redirect_to users_path
    else
      render 'new'
end
  end

  def show
    @user = User.find(params[:id])
  end

  def home
    @user = User.all
  end
end
