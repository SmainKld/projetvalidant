class EventsController < ApplicationController
  def new
    if logged_in?
      @event = Event.new
    else
      flash[:danger] = 'Vous devez être connecté pour créer un évènement'
      render new
    end
  end

  def create
    #A revoir
  end

  def index
    @events = Event.all
  end

  def show
    @event = Event.find(params[:id])
  end
end
